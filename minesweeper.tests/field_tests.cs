﻿using System;
using System.Security.Cryptography.X509Certificates;
using minesweeper.Game;
using NUnit.Framework;

namespace minesweeper.tests
{
    public class field_tests
    {

        [Test]
        public void generated_field_contains_acquired_count_of_mines()
        {
            var size = new Field {Width = 10, Height = 5};
            var field = Game.Game.GenerateField(size, 3);

            var actual = 0;

            for (var y = 0; y < size.Height; y++)
                
            {
                for (var x = 0; x < size.Width; x++)
                {
                    Console.Write(field[x, y].MineState == Game.Game.BOMB ? "X" : field[x, y].MineState.ToString());
                    if (field[x, y].MineState == Game.Game.BOMB)
                    {
                        actual++;
                    }
                }
                Console.WriteLine();
            }

            Assert.AreEqual(3, actual);
        }
    }
}
