﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace minesweeper.Game
{
    public interface IUserCommand
    {
        Cell Cell { get; }
    }

    public sealed class Mark : IUserCommand
    {
        public Mark(Cell cell)
        {
            Cell = cell;
        }

        public Cell Cell { get; }
    }

    public sealed class Unmark : IUserCommand
    {
        public Unmark(Cell cell)
        {
            Cell = cell;
        }

        public Cell Cell { get; }
    }

    public sealed class Check : IUserCommand
    {
        public Cell Cell { get; }
    }

    public interface IGameEvent
    {

    }

    public sealed class GameStarted : IGameEvent
    {
        public readonly Field Size;
        public readonly int MinesCount;

        public GameStarted(Field size, int minesCount)
        {
            Size = size;
            MinesCount = minesCount;
        }
    }

    public sealed class GameWon : IGameEvent
    {

    }

    public sealed class GameLost : IGameEvent
    {
        public readonly IReadOnlyCollection<Cell> WrongMarks;

        public GameLost(IReadOnlyCollection<Cell> wrongMarks)
        {
            WrongMarks = wrongMarks;
        }
    }

    public sealed class FieldChanges : IGameEvent
    {
        public readonly IReadOnlyCollection<IFieldChange> Changes;

        public FieldChanges(IReadOnlyCollection<IFieldChange> changes)
        {
            Changes = changes;
        }
    }

    public interface IFieldChange
    {
        Cell Cell { get; }
    }

    public sealed class Marked : IFieldChange
    {
        public Marked(Cell cell, int minesLeft)
        {
            Cell = cell;
            MinesLeft = minesLeft;
        }

        public Cell Cell { get; }
        public int MinesLeft { get; }
    }

    public sealed class Unmarked : IFieldChange
    {
        public Unmarked(Cell cell, int minesLeft)
        {
            Cell = cell;
            MinesLeft = minesLeft;
        }

        public Cell Cell { get; }
        public int MinesLeft { get; }
    }

    public sealed class Revealed : IFieldChange
    {
        public Revealed(Cell cell, int minesAround)
        {
            Cell = cell;
            MinesAround = minesAround;
        }

        public Cell Cell { get; }
        public int MinesAround { get; }
    }

    public struct CellState
    {
        public int MineState;
        public bool Marked;
        public bool Revealed;
    }

    public sealed class Game
    {
        public const int BOMB = -1;

        private readonly CellState[,] _field;
        private int MarksLeft;
        private long UnrevealedCells;
        

        public Game(Field size, int minesCount)
        {
            _field = GenerateField(size, minesCount);
            MarksLeft = minesCount;
            UnrevealedCells = size.Height * size.Width;
        }

        public IGameEvent HandleCommand(IUserCommand command)
        {
            if (command is Mark)
            {
                return HandleMark(command.Cell);
            }
            else if (command is Unmark)
            {
                return HandleUnmark(command.Cell);
            }
            else if (command is Check)
            {
                return HandleCheck(command.Cell);
            }
            else
            {
                throw new ArgumentException($"{command.GetType().Name} is unknown", nameof(command));
            }
        }

        private IGameEvent HandleUnmark(Cell commandCell)
        {
            if (_field[commandCell.X, commandCell.Y].Marked)
            {
                _field[commandCell.X, commandCell.Y].Marked = false;
                MarksLeft += 1;
                return new FieldChanges(new []{ new Unmarked(commandCell, MarksLeft)  });
            }

            return null;
        }

        private IGameEvent HandleCheck(Cell commandCell)
        {
            var cell = _field[commandCell.X, commandCell.Y];
            if (!cell.Revealed && !cell.Marked)
            {
                if (cell.MineState == -1)
                {
                    return new GameLost(GetWrongMarks(_field).ToArray());
                }
                else
                {

                }
            }

            return null;
        }

        private IEnumerable<Cell> GetWrongMarks(CellState[,] field)
        {
            throw new NotImplementedException();
        }

        private IGameEvent HandleMark(Cell commandCell)
        {
            if (!_field[commandCell.X, commandCell.Y].Marked)
            {
                _field[commandCell.X, commandCell.Y].Marked = true;
                MarksLeft -= 1;
                return new FieldChanges(new[] { new Marked(commandCell, MarksLeft) });
            }

            return null;
        }


        public static CellState[,] GenerateField(Field size, int minesCount)
        {
            var rnd = new Random();
            var field = new CellState[size.Width, size.Height];

            for (int i = 0; i < minesCount; i++)
            {
                int mineX, mineY;

                do
                {
                    mineX = rnd.Next(size.Width);
                    mineY = rnd.Next(size.Height);
                    // probably it's better to exclude fields
                } while (field[mineX, mineY].MineState == BOMB);

                field[mineX, mineY] = new CellState{ Marked = false, MineState = -1, Revealed = false };

                for (var nx = Math.Max(0, mineX - 1); nx <= Math.Min(size.Width - 1, mineX + 1); nx++)
                {
                    for (var ny = Math.Max(0, mineY - 1); ny <= Math.Min(size.Height - 1, mineY + 1); ny++)
                    {
                        if (field[nx, ny].MineState != BOMB) field[nx, ny].MineState += 1;
                    }
                }
            }

            return field;
        }
    }
}
