﻿namespace minesweeper.Game
{
    public struct Field
    {
        public int Width;
        public int Height;
    }

    public struct Cell
    {
        public int X;
        public int Y;
    }
}